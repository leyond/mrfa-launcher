#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle(TITLE);

    tabWidget = ui->tabWidget;

    availableModsRefreshButton = ui->availableModsRefreshButton;
    availableModsTreeWidget = ui->availableModsTreeWidget;

    presetsComboBox = ui->presetsComboBox;
    presetsListWidget = ui->presetsListWidget;

    arma3ExecutableEdit = ui->arma3ExecutableEdit;
    arma3ExecutableBrowseButton = ui->arma3ExecutableBrowseButton;
    arma3ExecutableAutoDetectButton = ui->arma3ExecutableAutoDetectButton;

    modsFolderEdit = ui->modsFolderEdit;
    modsFolderBrowseButton = ui->modsFolderBrowseButton;
    modsFolderAutoDetectButton = ui->modsFolderAutoDetectButton;

    dlcGroupBox = ui->dlcGroupBox;

    launchPresetComboBox = ui->launchPresetComboBox;
    launchButton = ui->launchButton;

    connect(tabWidget,&QTabWidget::currentChanged,this,&MainWindow::tabWidgetCurrentChanged);

    connect(availableModsRefreshButton,&QPushButton::clicked,this,&MainWindow::availableModsRefreshButtonClicked);

    connect(presetsComboBox,QOverload<int>::of(&QComboBox::currentIndexChanged),this,&MainWindow::presetsComboBoxCurrentIndexChanged);

    connect(arma3ExecutableBrowseButton,&QPushButton::clicked,this,&MainWindow::arma3ExecutableBrowseButtonClicked);
    connect(arma3ExecutableAutoDetectButton,&QPushButton::clicked,this,&MainWindow::arma3ExecutableAutoDetectButtonClicked);

    connect(modsFolderEdit,&QLineEdit::textChanged,this,&MainWindow::modsFolderEditTextChanged);
    connect(modsFolderBrowseButton,&QPushButton::clicked,this,&MainWindow::modsFolderBrowseButtonClicked);
    connect(modsFolderAutoDetectButton,&QPushButton::clicked,this,&MainWindow::modsFolderAutoDetectButtonClicked);

    connect(launchPresetComboBox,QOverload<int>::of(&QComboBox::currentIndexChanged),this,&MainWindow::launchPresetComboBoxCurrentIndexChanged);
    connect(launchButton,&QPushButton::clicked,this,&MainWindow::launchButtonClicked);

    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    availableModsRefreshButton->setIcon(style()->standardIcon(QStyle::SP_BrowserReload));

    QMap<QString,QVariant> settingsMap = getSettings();

    QString arma3Executable = settingsMap.value("arma3executable").toString();
    if (arma3Executable.isEmpty()) {
        arma3ExecutableAutoDetect();
    } else {
        setArma3Executable(arma3Executable);
    }

    QString modsFolder = settingsMap.value("modsfolder").toString();
    if (modsFolder.isEmpty()) {
        modsFolderAutoDetect();
    } else {
        setModsFolder(modsFolder);
    }

    QMap<QString, QVariant> appInfo = getAppInfo();

    if (appInfo.contains("dlcs")) {
        QList<QVariant> dlcs = appInfo.value("dlcs").toList();

        for (int i = 0; i < dlcs.size(); i++) {
            QMap<QString, QVariant> dlc = dlcs.at(i).toMap();

            if (dlc.contains("name") && dlc.contains("folder")) {
                QString folder = dlc.value("folder").toString();
                QString name = dlc.value("name").toString();

                QCheckBox *checkBox = addDLCOption(name);

                if (hasDLC(folder)) {
                    checkBox->setProperty("folder",QVariant(folder));
                    checkBox->setEnabled(true);

                    QString settingsKey = QString("dlc/%1").arg(folder);

                    if (!(settingsMap.contains(settingsKey)) || settingsMap.value(settingsKey).toBool()) {
                        checkBox->setCheckState(Qt::Checked);
                    } else {
                        checkBox->setCheckState(Qt::Unchecked);
                    }

                    connect(checkBox,&QCheckBox::stateChanged,this,&MainWindow::dlcheckboxStateChanged);
                } else {
                    checkBox->setEnabled(false);
                    checkBox->setCheckState(Qt::Unchecked);
                }
            }
        }
    }

    if (dlcGroupBox->layout()->count() == 0) {
        dlcGroupBox->setHidden(true);
    }

    loadMods();
}

QCheckBox * MainWindow::addDLCOption(QString name)
{
    QGridLayout *layout = qobject_cast<QGridLayout *>(dlcGroupBox->layout());

    if (layout != nullptr) {
        int noOfChildren = layout->count();

        int row = qFloor(noOfChildren / 2);
        int column = noOfChildren % 2;

        QCheckBox *checkBox = new QCheckBox(dlcGroupBox);
        checkBox->setText(name);

        layout->addWidget(checkBox,row,column);

        return checkBox;
    } else {
        return nullptr;
    }
}

QString MainWindow::joinPath(QStringList paths)
{
    QString path;

    foreach (QString p, paths) {
        if (path.isEmpty()) {
            path = p;
        } else {
            path = QDir(path).filePath(p);
        }
    }

    path = QDir::toNativeSeparators(QDir::cleanPath(path));

    return path;
}

bool MainWindow::hasDLC(QString folder)
{
    QString arma3Executable = arma3ExecutableEdit->text();

    if (!arma3Executable.isEmpty()) {
        QFileInfo fileInfo(arma3Executable);

        QDir dir(joinPath({QString("%1").arg(fileInfo.dir().path()), folder, "addons"}));

        if (dir.exists()) {
            return true;
        }
    }

    return false;
}

QMap<QString,QVariant> MainWindow::getSettings()
{
    QSettings settings;

    QMap<QString,QVariant> settingsMap;

    foreach (QString key, settings.allKeys()) {
        QVariant setting = settings.value(key);

        settingsMap.insert(key,setting);
    }

    return settingsMap;
}

void MainWindow::saveSettings()
{
    QSettings settings;

    settings.setValue("arma3executable",arma3ExecutableEdit->text());
    settings.setValue("modsfolder",modsFolderEdit->text());

    QStringList childGroups = settings.childGroups();

    if (childGroups.contains("dlc")) {
        settings.remove("dlc");
    }

    foreach (QObject *object, dlcGroupBox->children()) {
        QCheckBox *checkBox = qobject_cast<QCheckBox *>(object);

        if (checkBox != nullptr) {
            QString folder = checkBox->property("folder").toString();

            if (!folder.isEmpty()) {
                settings.setValue(QString("dlc/%1").arg(folder),checkBox->isChecked());
            }
        }
    }
}

void MainWindow::arma3ExecutableAutoDetect()
{
    QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\bohemia interactive\\arma 3",QSettings::NativeFormat);
    QString filename = settings.value("main","").toString();
    if (!filename.isEmpty()) {
        filename = joinPath({filename,"arma3_x64.exe"});
    }

    setArma3Executable(filename);
}

void MainWindow::modsFolderAutoDetect()
{
    QString currentPath = QDir::currentPath();
    setModsFolder(currentPath);
}

void MainWindow::loadMods()
{
    availableModsTreeWidget->clear();
    availableMods.clear();

    QString modsFolder = modsFolderEdit->text();

    QFileInfo modsFolderInfo(modsFolder);
    QString modsFoldername = modsFolderInfo.baseName();

    QTreeWidgetItem *rootItem = new QTreeWidgetItem(availableModsTreeWidget);
    rootItem->setText(0,modsFoldername);
    rootItem->setIcon(0,style()->standardIcon(QStyle::SP_DirHomeIcon));

    QStringList mods;

    QDirIterator dirIterator(modsFolder);
    while (dirIterator.hasNext()) {
        QString filename = dirIterator.next();

        QFileInfo fileInfo(filename);
        filename = fileInfo.baseName();

        if (filename.startsWith("@") && QDir(joinPath({fileInfo.filePath(),"addons"})).exists()) {
            mods.append(filename);
        }
    }

    std::sort(mods.begin(),mods.end());

    for (int i = 0; i < mods.size(); i++) {
        QString mod = mods.at(i);
        QTreeWidgetItem *item = new QTreeWidgetItem(rootItem);
        item->setText(0,mod);

        availableMods.append(mod);
    }

    availableModsTreeWidget->expandItem(rootItem);

    loadPresets();
}

void MainWindow::loadPresets()
{
    presetsComboBox->clear();
    launchPresetComboBox->clear();

    QMap<QString, QVariant> appInfo = getAppInfo();

    if (appInfo.contains("presets")) {
        QList<QVariant> presets = appInfo.value("presets").toList();

        for (int i = 0; i < presets.size(); i++) {
            QMap<QString,QVariant> preset = presets.at(i).toMap();

            if (preset.contains("name") && preset.contains("addons")) {
                QString name = preset.value("name").toString();
                QStringList addons = preset.value("addons").toStringList();

                std::sort(addons.begin(),addons.end());

                QJsonObject itemData;
                itemData.insert("addons",QJsonArray::fromStringList(addons));

                presetsComboBox->addItem(name,itemData);

                launchPresetComboBox->addItem(name);
            }
        }
    }

    presetsComboBox->addItem("Vanilla");
    launchPresetComboBox->addItem("Vanilla");

    loadCurrentPreset();
}

void MainWindow::loadCurrentPreset()
{
    presetsListWidget->clear();

    QJsonObject itemData = presetsComboBox->currentData().toJsonObject();
    if (!itemData.isEmpty()) {
        QJsonArray addons = itemData.value("addons").toArray();

        for (int i = 0; i < addons.size(); i++) {
            QString addon = addons.at(i).toString();

            QListWidgetItem *item = new QListWidgetItem(presetsListWidget);
            item->setText(addon);

            if (!availableMods.contains(addon)) {
                item->setForeground(Qt::red);
            }
        }
    }
}

void MainWindow::launchGame()
{
    QString arma3Executable = arma3ExecutableEdit->text();
    QString modsFolder = modsFolderEdit->text();

    QStringList arguments;
    arguments.append("-nosplash");
    arguments.append("-world=empty");
    arguments.append("-nologs");

    QMap<QString, QVariant> settingsMap = getSettings();

    foreach (QObject *object, dlcGroupBox->children()) {
        QCheckBox *checkBox = qobject_cast<QCheckBox *>(object);

        if (checkBox != nullptr && checkBox->isChecked()) {
            QString folder = checkBox->property("folder").toString();

            if (!folder.isEmpty()) {
                if (hasDLC(folder)) {
                    QString argument = QDir::toNativeSeparators(QString("-mod=%1").arg(folder));
                    arguments.append(argument);
                }
            }
        }
    }

    QJsonObject itemData = presetsComboBox->currentData().toJsonObject();
    if (!itemData.isEmpty()) {
        QJsonArray addons = itemData.value("addons").toArray();

        for (int i = 0; i < addons.size(); i++) {
            QString addon = addons.at(i).toString();

            QString argument = QString("-mod=%1").arg(joinPath({modsFolder,addon}));
            arguments.append(argument);
        }
    }

    QProcess process;
    process.setProgram(arma3Executable);
    process.setArguments(arguments);
    process.startDetached();
}

void MainWindow::setArma3Executable(QString text)
{
    arma3ExecutableEdit->setText(QDir::toNativeSeparators(text));
    arma3ExecutableEdit->setCursorPosition(0);
}

void MainWindow::setModsFolder(QString text)
{
    modsFolderEdit->setText(QDir::toNativeSeparators(text));
    modsFolderEdit->setCursorPosition(0);
}

void MainWindow::tabWidgetCurrentChanged(int index)
{
    if (index == 1) {
        arma3ExecutableEdit->setCursorPosition(0);
        modsFolderEdit->setCursorPosition(0);
    }
}

void MainWindow::availableModsRefreshButtonClicked()
{
    loadMods();
}

void MainWindow::presetsComboBoxCurrentIndexChanged(int index)
{
    launchPresetComboBox->blockSignals(true);
    launchPresetComboBox->setCurrentIndex(index);
    launchPresetComboBox->blockSignals(false);

    loadCurrentPreset();
}

void MainWindow::arma3ExecutableBrowseButtonClicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Programs (*.exe)"));

    QString arma3Executable = arma3ExecutableEdit->text();
    if (!arma3Executable.isEmpty()) {
        QFileInfo fileInfo(arma3Executable);
        if (fileInfo.exists()) {
            dialog.setDirectory(fileInfo.dir());
        }
    }

    if (dialog.exec()) {
        QStringList filenames = dialog.selectedFiles();
        QString filename = filenames.at(0);

        setArma3Executable(filename);

        saveSettings();
    }
}

void MainWindow::arma3ExecutableAutoDetectButtonClicked()
{
    arma3ExecutableAutoDetect();
    saveSettings();
}

void MainWindow::modsFolderEditTextChanged(const QString &text)
{
    loadMods();
}

void MainWindow::modsFolderBrowseButtonClicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::DirectoryOnly);

    QString modsFolder = modsFolderEdit->text();
    if (!modsFolder.isEmpty()) {
        QFileInfo fileInfo(modsFolder);

        if (fileInfo.exists() && fileInfo.isDir()) {
            dialog.setDirectory(fileInfo.dir());
        }
    }

    if (dialog.exec()) {
        QStringList filenames = dialog.selectedFiles();
        QString filename = filenames.at(0);

        setModsFolder(filename);

        saveSettings();
    }
}

void MainWindow::modsFolderAutoDetectButtonClicked()
{
    modsFolderAutoDetect();
    saveSettings();
}

void MainWindow::launchPresetComboBoxCurrentIndexChanged(int index)
{
    presetsComboBox->setCurrentIndex(index);
}

void MainWindow::launchButtonClicked()
{
    launchGame();
}

QMap<QString, QVariant> MainWindow::getAppInfo()
{
    QMap<QString, QVariant> infoMap;

    QString modsFolder = modsFolderEdit->text();
    QString infoFilename = joinPath({modsFolder,APPINFOFILENAME});

    QFile file(infoFilename);

    if (file.open(QIODevice::ReadOnly)) {
        QByteArray data = file.readAll();

        QJsonDocument jsonDocument(QJsonDocument::fromJson(data));
        if (!jsonDocument.isEmpty()) {
            QJsonObject jsonObject = jsonDocument.object();

            foreach (QString key, jsonObject.keys()) {
                QJsonValue jsonValue = jsonObject.value(key);

                infoMap.insert(key,jsonValue.toVariant());
            }
        }
    }

    return infoMap;
}

void MainWindow::dlcheckboxStateChanged(int state)
{
    saveSettings();
}
