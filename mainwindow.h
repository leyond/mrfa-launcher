#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QtCore>
#include <QtWidgets>

#define APPNAME "mrfa-launcher"
#define TITLE "Marine Rotation Force - Asia"
#define APPINFOFILENAME "mrfa-launcher.json"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QTabWidget *tabWidget;

    QToolButton *availableModsRefreshButton;
    QTreeWidget *availableModsTreeWidget;

    QComboBox *presetsComboBox;
    QListWidget *presetsListWidget;

    QLineEdit *arma3ExecutableEdit;
    QPushButton *arma3ExecutableBrowseButton;
    QPushButton *arma3ExecutableAutoDetectButton;

    QLineEdit *modsFolderEdit;
    QPushButton *modsFolderBrowseButton;
    QPushButton *modsFolderAutoDetectButton;

    QGroupBox *dlcGroupBox;

    QComboBox *launchPresetComboBox;
    QPushButton *launchButton;

    QStringList availableMods;

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void init();

    QMap<QString,QVariant> getSettings();
    QString joinPath(QStringList paths);
    QCheckBox * addDLCOption(QString name);
    bool hasDLC(QString folder);
    void saveSettings();
    void arma3ExecutableAutoDetect();
    void modsFolderAutoDetect();
    void loadMods();
    void loadPresets();
    void loadCurrentPreset();
    void launchGame();
    void setArma3Executable(QString text);
    void setModsFolder(QString text);
    QMap<QString, QVariant> getAppInfo();

    void tabWidgetCurrentChanged(int index);
    void availableModsRefreshButtonClicked();
    void presetsComboBoxCurrentIndexChanged(int index);
    void arma3ExecutableBrowseButtonClicked();
    void arma3ExecutableAutoDetectButtonClicked();
    void modsFolderEditTextChanged(const QString &text);
    void modsFolderBrowseButtonClicked();
    void modsFolderAutoDetectButtonClicked();
    void dlcheckboxStateChanged(int state);
    void launchPresetComboBoxCurrentIndexChanged(int index);
    void launchButtonClicked();
};

#endif // MAINWINDOW_H
